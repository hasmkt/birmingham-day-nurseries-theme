<?php get_template_part('templates/page', 'header'); ?>

<div class="container">
  <section class="row justify-content-center">
    <article class="page-content col-11 px-0 pb-7">

      <p>
        <?php _e('Sorry, but the page you were trying to view does not exist. Please try searching or use the navigation menu above.', 'sage'); ?>
      </p>

      <?php get_search_form(); ?>

    </article>
  </section>
</div>
