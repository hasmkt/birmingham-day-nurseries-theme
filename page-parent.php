<?php
/**
 * Template Name: Parent Corner
 */
?>
<?php while (have_posts()) : the_post(); ?>
  <?php
    if (SwpmMemberUtils::is_member_logged_in()) :

    get_template_part('templates/page', 'header');
  ?>
    <section>
      <?php
        if ( is_page( 'parent-corner' ) ) {
          get_template_part('templates/components/parentnews');
        }
        get_template_part('templates/components/parent', 'downloads');
        get_template_part('templates/components/page', 'carousel');
      ?>

    <?php else : ?>
      <div class="container">
        <section class="row justify-content-center">
          <article class="page-content col-11 px-0 pb-7">

            <?php echo do_shortcode("[swpm_login_form]"); ?>

          </article>
        </section>
      </div>

    </section>
  <?php endif; // is logged in?>

<?php endwhile; ?>
