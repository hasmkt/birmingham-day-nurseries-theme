<?php
/**
 * Template Name: Apply for a Place
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <div class="container-fluid">
    <section class="d-flex justify-content-center">
      <article class="col-12 col-sm-10 px-0">
        <?php the_content(); ?>
      </article>
    </section>
  </div>
<?php endwhile; ?>
