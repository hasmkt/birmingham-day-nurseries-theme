<?php use Roots\Sage\Titles; use Roots\Sage\acf;  use Roots\Sage\extras;

  $layout_option = get_field('header_background');

  if ( !is_page(array('login', 346)) ) :
?>

<div class="row child-nav-container align-items-center">
  <div class="col-12 col-lg-2">
    <?php get_template_part('templates/components/breadcrumb', ''); ?>
  </div>
  <div class="col-lg-8 d-none d-lg-inline-block">
    <nav id="child-nav" class="navbar justify-content-center align-items-lg-center">
      <?php
        if ( has_nav_menu('primary_navigation') ) :
          wp_nav_menu( array(
            'menu'        => 'Header',
            'sub_menu'    => true,
            'menu_class'  => 'nav'
          ) );
        endif;
      ?>
    </nav>
  </div>
</div>

<header class="page-header text-center bg-is-off-white <?php echo $layout_option; ?>" <?php echo $show_bgimage = ($layout_option == 'image') ? 'style="background-image: url(' . get_field('header_image') .  '); background-position: center;"' : null ; ?>>

  <div class="container">

    <div class="row">
      <div class="col-10 col-lg-9 px-lg-4 mx-auto">
        <?php if ( have_rows('header_options') ) :

          while ( have_rows('header_options') ) : the_row();
        ?>

          <?php if ( get_row_layout() == 'image' ) :
            $image = get_sub_field('image');
          ?>
            <img class="logo" <?php acf\ar_responsive_image($image, 'thumb-1000', '100%', false); ?> alt="<?php echo get_the_title($image); ?>" >
          <?php else: ?>
            <h1 class="<?php if(isset($image)) { echo 'sr-only'; } ?>"><?= Titles\title(); ?></h1>
          <?php endif; //get_row_layout() == 'image' ?>

          <?php if ( get_row_layout() == 'content' ) :
            $content = get_sub_field('content');
          ?>
            <p><?php echo $content; ?></p>
          <?php endif; //get_row_layout() == 'content' ?>

        <?php endwhile; //have_rows('header_options') ?>

        <?php else: ?>
          <h1><?= Titles\title(); ?></h1>
        <?php endif; //have_rows('header_options') ?>

      </div>
    </div>

  </div> <!-- /.container -->

  <?php if ( $layout_option == 'layout_1' ) : ?>

   <span class="icon icon-reveal green-circle-1"></span>
   <span class="icon icon-reveal green-circle-2"></span>
   <span class="icon icon-reveal blue-dot-1"></span>
   <span class="icon icon-reveal green-blob-1"></span>
   <span class="icon icon-reveal blue-wave-1"></span>
   <span class="icon icon-reveal pink-zigzag-1"></span>
   <span class="icon icon-reveal yellow-dash-4"></span>
   <span class="icon icon-reveal blue-square-1"></span>
   <span class="icon icon-reveal purple-curve-1"></span>
   <span class="icon icon-reveal pink-line-1"></span>
   <span class="icon icon-reveal yellow-wave-1"></span>

 <?php elseif ( $layout_option == 'layout_2' ) : ?>

   <span class="icon icon-reveal purple-square-1"></span>
   <span class="icon icon-reveal pink-square-2"></span>
   <span class="icon icon-reveal blue-plus-1"></span>
   <span class="icon icon-reveal blue-plus-1 alt"></span>
   <span class="icon icon-reveal green-dot-1"></span>
   <span class="icon icon-reveal green-square-1"></span>
   <span class="icon icon-reveal purple-zigzag-1"></span>
   <span class="icon icon-reveal yellow-dash-4"></span>
   <span class="icon icon-reveal yellow-dash-3"></span>
   <span class="icon icon-reveal blue-wave-1"></span>
   <span class="icon icon-reveal pink-triangle-1"></span>
   <span class="icon icon-reveal green-blob-1"></span>
   <span class="icon icon-reveal yellow-square-1"></span>
   <span class="icon icon-reveal purple-plus-1"></span>

 <?php elseif ( $layout_option == 'layout_3' ) : ?>

   <span class="icon icon-reveal blue-circle-1"></span>
   <span class="icon icon-reveal green-blob-1"></span>
   <span class="icon icon-reveal pink-triangle-1"></span>
   <span class="icon icon-reveal blue-zigzag-1"></span>
   <span class="icon icon-reveal purple-wave-1"></span>
   <span class="icon icon-reveal green-plus-1"></span>
   <span class="icon icon-reveal blue-curve-3"></span>
   <span class="icon icon-reveal green-circle-2"></span>
   <span class="icon icon-reveal green-circle-2 alt"></span>
   <span class="icon icon-reveal yellow-wave-1"></span>
   <span class="icon icon-reveal blue-dash-4"></span>
   <span class="icon icon-reveal purple-square-2"></span>
   <span class="icon icon-reveal pink-square-1"></span>


 <?php elseif ( $layout_option == 'layout_4' ) : ?>

   <span class="icon icon-reveal pink-square-1"></span>
   <span class="icon icon-reveal blue-circle-1"></span>
   <span class="icon icon-reveal green-triangle-1"></span>
   <span class="icon icon-reveal green-curve-3"></span>
   <span class="icon icon-reveal blue-circle-2"></span>
   <span class="icon icon-reveal blue-square-1"></span>
   <span class="icon icon-reveal blue-plus-1"></span>
   <span class="icon icon-reveal green-dot-1"></span>
   <span class="icon icon-reveal yellow-dash-4"></span>
   <span class="icon icon-reveal purple-curve-1"></span>
   <span class="icon icon-reveal yellow-square-1"></span>
   <span class="icon icon-reveal green-circle-2"></span>
   <span class="icon icon-reveal green-circle-2 alt"></span>
   <span class="icon icon-reveal blue-triangle-1"></span>
   <span class="icon icon-reveal purple-square-2"></span>
   <span class="icon icon-reveal pink-triangle-1"></span>

 <?php elseif ( $layout_option == 'layout_5' ) : ?>

   <span class="icon icon-reveal blue-line-1"></span>
   <span class="icon icon-reveal green-zigzag-1"></span>
   <span class="icon icon-reveal blue-circle-2"></span>
   <span class="icon icon-reveal purple-plus-1"></span>
   <span class="icon icon-reveal purple-square-1"></span>
   <span class="icon icon-reveal green-blob-1"></span>
   <span class="icon icon-reveal yellow-wave-1"></span>
   <span class="icon icon-reveal yellow-dash-3"></span>
   <span class="icon icon-reveal blue-curve-3"></span>
   <span class="icon icon-reveal green-square-1"></span>
   <span class="icon icon-reveal pink-plus-1"></span>
   <span class="icon icon-reveal purple-square-2"></span>
   <span class="icon icon-reveal green-dot-1"></span>
   <span class="icon icon-reveal blue-triangle-1"></span>
   <span class="icon icon-reveal yellow-square-1"></span>

 <?php elseif ( $layout_option == 'forest_school' ) : ?>

   <span class="icon icon-reveal blue-dash-3"></span>
   <span class="icon icon-reveal blue-dash-4"></span>
   <span class="icon icon-reveal tree-3"></span>
   <span class="icon icon-reveal tree-3 alt"></span>
   <span class="icon icon-reveal tree-4"></span>
   <span class="icon icon-reveal tree-5"></span>
   <span class="icon icon-reveal tree-5 alt"></span>

 <?php endif; ?>


</header>
<?php endif; ?>
