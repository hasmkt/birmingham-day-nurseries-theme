<?php
  $intro = get_field('intro');

  if ( $intro ) :
 ?>
 <div class="container">
   <section class="pt-lg-3 block-intro scroll-animate">
     <article class="text-center">

       <div class="row">
         <div class="col-8 p-0 mx-auto">
           <h2><?php echo $intro['heading']; ?></h2>
         </div>
       </div>

       <div class="row divider divider-lg-none block-intro__content">
         <div class="col-11 col-md-8 mx-auto bg-is-yellow move-up">
           <p><?php echo $intro['text_area']; ?></p>
         </div>
       </div>

     </article>
     <span class="icon icon-reveal pink-triangle-1"></span>
     <span class="icon icon-reveal green-curve-3"></span>
     <span class="icon icon-reveal purple-zigzag-1"></span>
     <span class="icon icon-reveal green-zigzag-1"></span>
     <span class="icon icon-reveal blue-plus-1"></span>

   </section> <!-- /.block-intro -->
 </div> <!-- /.container -->
<?php endif; ?>
