<div class="container">
  <?php use Roots\Sage\acf;
    // check if the flexible content field has rows of data
    if ( have_rows('extend_content') ) :
      // loop through the rows of data
      $count = 0;
      while ( have_rows('extend_content') ) : the_row();
  ?>

    <?php
      // Content area
      // check current row layout
      if( get_row_layout() == 'content_area' ) :
    ?>
      <section class="row justify-content-center">
        <article class="page-content col-11 px-0 pb-7">
          <?php the_sub_field('content'); ?>
        </article>
      </section>
    <?php endif; ?>

    <?php
      // Feature content
      // check current row layout
      if( get_row_layout() == 'feature_content' ) :

        $heading = get_sub_field('heading');
        $text_area = get_sub_field('content');
    ?>
    <section class="row feature">
      <div class="col-11 col-sm-12 pb-3 mx-auto">
      <div class="feature-container move-up-reset">
        <article class="text-center">

          <div class="col-11 col-sm-6 px-3 mx-auto bg-is-white">
            <h2><?php echo $heading; ?></h2>
          </div>

          <div class="col-11 p-0 pb-5 mx-auto">
            <p><?php echo $text_area; ?></p>
          </div>

          <?php
            // Button
            if ( have_rows('button') ) : while ( have_rows('button') ) : the_row();

              $text = get_sub_field('button_text');
              $link = get_sub_field('button_link');
          ?>

            <?php if ( $text && $link ) : ?>
              <a class="button bg-is-blue b-is-blue" href="<?php echo $link; ?>"><?php echo $text; ?></a>
            <?php endif; ?>

          <?php endwhile; endif; //have_rows('button') ?>

        </article>
      </div> <!-- /.col-11 -->
      </div> <!-- /.col-11 -->
    </section>
    <?php endif; //get_row_layout() == 'feature_content' ?>

    <?php
      // Nurseries info
      // check current row layout
      if( get_row_layout() == 'nursery' ) :

        $logo = get_sub_field('logo');
        $text_area = get_sub_field('content');
        $link = get_sub_field('link');
    ?>
      <section class="row justify-content-center nursery-feature">
        <article class="page-content col-11 col-lg-12">

          <a href="<?php echo $link; ?>">
            <div class="row justify-content-md-around">
              <div class="col-9 col-sm-4 px-1 pb-7">
                <?php if ( $logo ) : ?>
                  <img class="lozad" <?php acf\ar_responsive_image($logo, 'thumb-640', '100%'); ?> alt="<?php echo get_the_title($logo) ?>" >
                <?php endif; ?>
              </div>

              <div class="col-sm-7 px-1">
                <p><?php echo $text_area; ?></p>
              </div>
            </div> <!-- /.row -->
          </a>

        </article>
      </section>
    <?php endif; //get_row_layout() == 'nursery' ?>

  <?php $count++; endwhile; endif; //have_rows('extend_content') ?>
</div>
