<?php use Roots\Sage\acf;

  $header = get_field('header');


  if ( isset($header) && (have_rows('info') ) ) :
?>

<div class="container-fluid">
  <section class="nursery-info bg-is-off-white">
    <div class="container">

      <h2 class="pb-7 pb-lg-0"><?php echo $header; ?></h2>
      <ul class="row">

        <?php
        while ( have_rows('info') ) : the_row();

        $bullet_point = get_sub_field('bullet');
        ?>

        <li class="col-12 col-md-6"><?php echo $bullet_point; ?></li>

      <?php endwhile; ?>

      </ul>

      <?php if ( is_page('The Elms Day Nursery') ) : ?>

        <!-- Opening times for The Elms -->
        <?php if ( have_rows('opening_times', 'option') ) : ?>
          <div class="row justify-content-center justify-content-lg-start opening-times">

            <?php
              while ( have_rows('opening_times', 'option') ) : the_row();

              $mon_fri = get_sub_field_object('monday_friday', 'option');
              $morning = get_sub_field_object('morning_session', 'option');
              $afternoon = get_sub_field_object('afternoon_session', 'option');
            ?>
              <?php if ($mon_fri) : ?>
                <div class="col-11 col-md-4 p-2 p-md-3 p-xl-4 bg-is-white"><?php echo '<span>' . $mon_fri['label'] . '</span><span>' . $mon_fri['value'] . '</span>'; ?></div>
              <?php endif; ?>
              <?php if ($morning) : ?>
                <div class="col-11 col-md-4 p-2 p-md-3 p-xl-4 bg-is-white"><?php echo '<span>' . $morning['label'] . '</span><span>' . $morning['value'] . '</span>'; ?></div>
              <?php endif; ?>
              <?php if ($afternoon) : ?>
                <div class="col-11 col-md-4 p-2 p-md-3 p-xl-4 bg-is-white"><?php echo '<span>' . $afternoon['label'] . '</span><span>' . $afternoon['value'] . '</span>'; ?></div>
              <?php endif; ?>
            <?php endwhile; ?>

          </div>
        <?php endif; ?>

      <?php elseif ( is_page('The Oaks Day Nursery') ) : ?>

        <!-- Opening times for The Oaks -->
        <?php if ( have_rows('opening_times_oaks', 'option') ) : ?>
          <div class="row justify-content-center justify-content-lg-start opening-times">

            <?php
              while ( have_rows('opening_times_oaks', 'option') ) : the_row();

              $mon_fri = get_sub_field_object('monday_friday', 'option');
              $morning = get_sub_field_object('morning_session', 'option');
              $afternoon = get_sub_field_object('afternoon_session', 'option');
            ?>
              <?php if ($mon_fri) : ?>
                <div class="col-11 col-md-4 p-2 p-md-3 p-xl-4 bg-is-white"><?php echo '<span>' . $mon_fri['label'] . '</span><span>' . $mon_fri['value'] . '</span>'; ?></div>
              <?php endif; ?>
              <?php if ($morning) : ?>
                <div class="col-11 col-md-4 p-2 p-md-3 p-xl-4 bg-is-white"><?php echo '<span>' . $morning['label'] . '</span><span>' . $morning['value'] . '</span>'; ?></div>
              <?php endif; ?>
              <?php if ($afternoon) : ?>
                <div class="col-11 col-md-4 p-2 p-md-3 p-xl-4 bg-is-white"><?php echo '<span>' . $afternoon['label'] . '</span><span>' . $afternoon['value'] . '</span>'; ?></div>
              <?php endif; ?>
            <?php endwhile; ?>

          </div>
        <?php endif; ?>

      <?php elseif ( is_page('The Maples Day Nursery') ) : ?>

        <!-- Opening times for The Maples -->
        <?php if ( have_rows('opening_times_maples', 'option') ) : ?>
          <div class="row justify-content-center justify-content-lg-start opening-times">

            <?php
              while ( have_rows('opening_times_maples', 'option') ) : the_row();

              $mon_fri = get_sub_field_object('monday_friday', 'option');
              $morning = get_sub_field_object('morning_session', 'option');
              $afternoon = get_sub_field_object('afternoon_session', 'option');
            ?>
              <?php if ($mon_fri) : ?>
                <div class="col-11 col-md-4 p-2 p-md-3 p-xl-4 bg-is-white"><?php echo '<span>' . $mon_fri['label'] . '</span><span>' . $mon_fri['value'] . '</span>'; ?></div>
              <?php endif; ?>
              <?php if ($morning) : ?>
                <div class="col-11 col-md-4 p-2 p-md-3 p-xl-4 bg-is-white"><?php echo '<span>' . $morning['label'] . '</span><span>' . $morning['value'] . '</span>'; ?></div>
              <?php endif; ?>
              <?php if ($afternoon) : ?>
                <div class="col-11 col-md-4 p-2 p-md-3 p-xl-4 bg-is-white"><?php echo '<span>' . $afternoon['label'] . '</span><span>' . $afternoon['value'] . '</span>'; ?></div>
              <?php endif; ?>
            <?php endwhile; ?>

          </div>
        <?php endif; ?>

      <?php endif; ?>





    </div> <!-- /.container -->
  </section>
</div> <!-- /.container-fluid -->
<?php  endif; ?>
