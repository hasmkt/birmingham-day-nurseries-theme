<?php if (!have_posts()) : ?>
  <div class="container">
    <section class="row justify-content-center">
      <article class="page-content col-11 px-0 pb-7">
        <p>
          <?php _e('Sorry, no results were found.', 'sage'); ?>
        </p>
        <?php get_search_form(); ?>
      </article>
    </section>
  </div>
<?php endif; ?>
