<?php
// Testimonial
 if ( have_rows('testimonial') ) :
   while ( have_rows('testimonial') ) : the_row();

     if ( get_row_layout() == 'testimonial' ) :

       $image = get_sub_field('image');
       $quote = get_sub_field('quote');
       $name = get_sub_field('name');
       $relation = get_sub_field('relation');
  ?>
  <article class="panel__testimonial">
    <div class="row">
        <?php if ( $image ) : ?>
          <div class="col-3">
            <img class="mw-100 lozad" <?php acf\ar_responsive_image($image, 'thumb-640', '100%'); ?> alt="<?php echo get_the_title($image) ?>" >
          </div>
        <?php endif; ?>
      <div class="col-9 mx-auto ml-md-0">
        <blockquote class="col-11 mx-auto mx-md-0 px-md-0">
          <?php echo $quote; ?>
        </blockquote>
        <cite>
          <?php echo $name; ?><span><?php echo $relation; ?></span>
        </cite>
      </div>
    </div>
  </article>

<?php endif; //get_row_layout() == 'testimonial' ?>
<?php endwhile; endif; //have_rows('testimonial') ?>
