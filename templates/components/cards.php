<?php use Roots\Sage\acf; ?>
<div class="container card-column-container">
  <?php get_template_part('templates/components/cards', 'intro'); ?>
  <section class="row card-columns mt-md-7">
    <?php
      // check if the flexible content field has rows of data
      if ( have_rows('cards') ) :
        // loop through the rows of data
        while ( have_rows('cards') ) : the_row();
    ?>

        <?php
          // Card
          // check current row layout
          if( get_row_layout() == 'card' ) :

            $bg_colour = get_sub_field('background_colour');
            $post_object = get_sub_field('link');

            if ( $post_object ) :
            // override $post
            $post = $post_object;
            setup_postdata( $post );

            $image = get_post_thumbnail_id();
        ?>
          <div class="card move-up-reset">
            <div class="bg-is-<?php echo $bg_colour; ?> pt-md-7 px-md-0">
              <a href="<?php the_permalink(); ?>">
                <?php if ( $image ) : ?>
                  <div class="card-img__container mx-auto">
                    <img class="card-img lozad" <?php acf\ar_responsive_image($image, 'thumb-640', '100%'); ?> alt="<?php echo get_the_title($image); ?>">
                  </div>
                <?php endif; ?>
                <h2 class="card-title"><?php the_title(); ?></h2>
              </a>

              <?php if ($bg_colour == 'green') : ?>
                <span class="icon dark-dot-1"></span>
                <span class="icon dark-zigzag-1"></span>
                <span class="icon dark-circle-1"></span>
                <span class="icon dark-plus-1"></span>
              <?php elseif ($bg_colour == 'blue' || $bg_colour == 'purple' || $bg_colour == 'orange') : ?>
                <span class="icon dark-zigzag-1"></span>
                <span class="icon dark-circle-1"></span>
                <span class="icon dark-plus-1"></span>
              <?php elseif ($bg_colour == 'yellow') : ?>
                <span class="icon dark-zigzag-1"></span>
                <span class="icon dark-curve-1"></span>
                <span class="icon dark-plus-1"></span>
              <?php endif; ?>
            </div>
          </div>
        <?php wp_reset_postdata(); endif; //if ( $post_objects ) ?>
        <?php endif; //get_row_layout() == 'card' ?>

    <?php endwhile; endif; //have_rows('cards') ?>

    <span class="icon icon-reveal pink-plus-1"></span>
    <span class="icon icon-reveal green-triangle-1"></span>
    <span class="icon icon-reveal purple-curve-1"></span>
    <span class="icon icon-reveal yellow-plus-1"></span>
    <span class="icon icon-reveal pink-zigzag-1"></span>
  </section>
</div>
