<?php 
  if ( have_rows('notice', 'option') ) {

    while ( have_rows('notice', 'option') ){
      the_row();

      $notice_content = get_sub_field('notice_content', 'option');
      $notice_active = get_sub_field('notice_active', 'option');
    } 
  }

  if ( $notice_active && !isset($_COOKIE['dismissed-notice']) ) :
?>

	<div class="notice">
    <button class="close fas fa-times" aria-label="Close alert" type="button"></button>
    <?php echo $notice_content; ?>
	</div>

<?php endif; ?>