<div class="modal" id="mainNav" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <header class="modal-header">
        <nav class="navbar d-none d-md-block p-md-0 pl-7">
          <div class="secondary-menu p-md-0">
            <?php
              if (has_nav_menu('secondary_navigation')) :
                wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav']);
              endif;

              dynamic_sidebar('sidebar-footer');
            ?>

          </div>
        </nav>

        <button type="button" class="close navbar-toggler" data-dismiss="modal" aria-label="Close">
          <span class="hamburger-inner"></span>
        </button>
      </header>

      <div class="modal-body mt-md-6">
        <nav>
          <?php
            if ( has_nav_menu('main_navigation') ) :
              wp_nav_menu(['theme_location' => 'main_navigation', 'menu_class' => 'navbar-nav flex-row flex-wrap']);
            endif;
          ?>
        </nav>

      </div>

      <footer class="modal-footer">
        <nav class="navbar p-md-0">
          <div class="secondary-menu d-block d-md-none">
            <?php
              if (has_nav_menu('secondary_navigation')) :
                wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav justify-content-center']);
              endif;

              dynamic_sidebar('sidebar-footer');
            ?>

          </div>
          <?php get_template_part('templates/components/logo', ''); ?>
        </nav>

      </footer>

    </div> <!-- /.modal-content -->
  </div> <!-- /.modal-dialog -->
</div> <!-- /#mainNav -->
