<?php /* is it a page */
if( is_page() ) {
	global $post;

  $anc = get_post_ancestors( $post->ID );
  $id = ($anc) ? $anc[count($anc)-1]: $post->ID;
  $parent_id = get_post( $id );
  $parent_title = $parent_id -> post_title;
  $parent_slug = $parent_id -> post_name;
}

if (
	is_page() &&
	// Check if page has a parent
	$post->post_parent &&
	// Check that page is not equal to the login page for the parent corner
	$post -> post_title != 'Login'
	) : ?>

  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="<?php echo esc_url('/'.$parent_slug.'/');?>">
          <span class="fas fa-long-arrow-alt-left"></span><?php echo esc_html_e($parent_title, 'textdomain'); ?>
        </a>
      </li>
    </ol>
  </nav>

<?php endif; //(is_page() && $post->post_parent ?>
