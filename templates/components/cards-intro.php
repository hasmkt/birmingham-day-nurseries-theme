<?php
  // check if the flexible content field has rows of data
  if ( have_rows('cards') ) :
    // loop through the rows of data
    while ( have_rows('cards') ) : the_row();
?>

  <?php
    // Content area
    // check current row layout
    if( get_row_layout() == 'content_area' ) :

      $heading = get_sub_field('heading');
      $text_area = get_sub_field('text_area');
  ?>
    <section class="pt-lg-3 block-intro">
      <article class="text-center">

          <div class="row">
             <div class="col-8 p-0 mx-auto">
               <h2><?php echo $heading; ?></h2>
             </div>
           </div>

           <div class="row block-intro__content">
             <div class="col-12 col-md-8 mx-auto pb-7">
              <p><?php echo $text_area; ?></p>
            </div>
          </div>

      </article>
    </section>
  <?php endif; //get_row_layout() == 'content_area' ?>
<?php endwhile; endif; //have_rows('cards') ?>
