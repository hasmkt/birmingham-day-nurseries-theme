<?php
  use Roots\Sage\acf;

  $post_type = 'parentnews'; ?>
  <div class="container">
    <section>
      <div class="row">
        <div class="col-12">
          <header>
            <h2 class="title"><?php esc_html_e('Nursery Announcements', 'sage'); ?></h2>
          </header>
        </div>
      </div>

      <div class="posts col-12">
        <div class="row">

          <?php
            $the_query = new WP_Query( array('post_type' => $post_type) );
            if( $the_query -> have_posts() ) : $the_query -> the_post();
          ?>

          <div class="col-12 col-sm-6 pl-sm-0 pr-sm-4 pr-lg-6">
            <?php
            // Start a new loop and set posts per page to one to show only the latest post
            $the_query = new WP_Query( array('post_type' => $post_type, 'posts_per_page' => 1) );
            if( $the_query -> have_posts() ) :
            while ( $the_query -> have_posts() ) : $the_query -> the_post(); ?>
              <article <?php post_class('latest-post');?> >
                <header>
                  <?php
                  if ( has_post_thumbnail() ) : ?>

                  <a href="<?php the_permalink(); ?>">
                    <figure>
                      <?php the_post_thumbnail(
                        'large',
                        array( 'class' => ''
                          )
                        ); ?>
                    </figure>
                  </a>

                  <?php endif;?>
                  <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                </header>

                <footer>
                  <?php get_template_part('templates/components/parentnews', 'list-categories') ?>
                </footer>
              </article>
            <?php endwhile; endif; ?>
          </div>

          <div class="post-list col-12 col-sm-6 pr-sm-0 pl-sm-4 pl-lg-6 mb-6 mb-sm-3">
            <?php
              // Start a new loop and set posts per page to one to show only the latest post
              $the_query = new WP_Query( array('post_type' => $post_type, 'posts_per_page' => 4, 'offset' => 1) );
              $count = 0;
              if( $the_query -> have_posts() ) :
              while ( $the_query -> have_posts() ) : $the_query -> the_post(); ?>

              <article>
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <footer>
                  <?php get_template_part('templates/components/parentnews', 'list-categories') ?>
                </footer>
              </article>
            <?php $count++; endwhile; endif; wp_reset_postdata(); ?>

            <?php if ( $count >= 4) : ?>
              <a class="button b-is-yellow" href="<?= esc_url(home_url('/parent-corner/news/')); ?>">View all</a>
            <?php endif; ?>

          </div>

          <?php else: ?>
            <div class="col-12">

              <p class="text-center"><?php esc_html_e('There are currently no announcements', 'sage'); ?></p>
            </div>

          <?php endif; // if posts exist?>
        </div>
      </div>
    </section>
  </div>
