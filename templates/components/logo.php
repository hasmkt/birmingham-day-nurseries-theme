<?php use Roots\Sage\acf;

  $image = get_field('logo', 'option');

  if ( $image ) :

  ?>
  <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
    <img <?php acf\ar_responsive_image($image, 'thumb-640', '100%', false); ?> alt="<?php echo get_the_title($image); ?>">
  </a>
<?php endif; ?>
