<?php use Roots\Sage\acf; ?>
<div class="useful-downloads bg-is-off-white">
  <div class="container">
    <section>

      <?php
      // get scf documents field on the parent corner page (note this doesn't include page id)
        $documents = get_field('documents');

        if( $documents ) :
      ?>
      <div class="row">
        <div class="col-12">
          <header>
            <h2 class="title"><?php echo $documents['title']; ?></h2>
          </header>
        </div>
      </div>

      <div class="row justify-content-md-between">
        <div class="col-12 col-sm-6 px-sm-0">
          <?php echo $documents['content']; ?>
        </div>
      <?php endif; // have_rows('documents') ?>

      <?php
      // include the id of documents page so acf can display the field despite it being on another page
        $doc_page_id = 374;

        if( have_rows('documents', $doc_page_id) ) : ?>
          <div class="col-12 col-sm-5 px-sm-0 zindex-1">
            <ul class="documents-list">
              <?php
                $count = 1;
                while( have_rows('documents', $doc_page_id) ) : the_row();
                $file = get_sub_field('file');
              ?>
                  <li class="<?php echo $count;?>"><a href="<?php echo $file['url']; ?>" target="_blank"><span class="far fa-file-alt"></span><?php echo $file['title']; ?></a></li>

                  <?php
                  // if there are more than 4 documents print a show more link and break out of the loop
                  if ( $count >= 4 ) {

                    echo '<a class="button b-is-yellow" href="' . esc_url(home_url('/parent-corner/documents/')) . '">View all</a>';

                    break;
                  } ?>

              <?php $count++; endwhile; // have_rows('documents, $doc_page_id') ?>
            </ul>
          </div>
        <?php endif; // have_rows('documents, $doc_page_id') ?>
      </div>
    </section>
  </div>
</div>
