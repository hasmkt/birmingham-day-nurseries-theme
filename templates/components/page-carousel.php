<?php use Roots\Sage\acf;

  if ( have_rows('gallery') ) :
    $title = get_field('carousel_title');
?>
  <div class="container-fluid">
    <section class="page-carousel">
      <div id="page-carousel" class=" carousel slide" data-ride="carousel">

        <ol class="carousel-indicators">
          <?php
            $count = 0;
            while ( have_rows('gallery') ) : the_row();
          ?>
            <li data-target="#page-carousel" data-slide-to="<?php echo $count;?>" class="<?php if ( $count == 0 ) { echo 'active';} ?>"></li>
          <?php $count++; endwhile; ?>
        </ol>

        <div class="carousel-inner">
          <?php
            $count = 0;
            while ( have_rows('gallery') ) : the_row();

            $bg_colour = get_sub_field('background_colour');
            $image = get_sub_field('image');
          ?>

          <div class="carousel-item bg-is-<?php echo $bg_colour; ?> <?php if ( $count == 0 ) { echo 'primary active';} ?>" >
            <?php if ( $title ) : ?>
              <h2 class="title <?php if($bg_colour == 'yellow') {echo 'bg-after-is-blue';}?>"><?php echo $title; ?></h2>
            <?php endif; ?>
              <img class="pb-lg-5 lozad" src="<?php echo get_template_directory_uri().'/assets/images/placeholder.png' ?>" <?php acf\ar_responsive_image($image, 'thumb-640', '100%'); ?> alt="<?php echo get_the_title($image) ?>" >

              <?php
                if ( have_rows('caption') ) :

                  while ( have_rows('caption') ) : the_row();

                  if ( get_row_layout() == 'caption_with_image' ) :

                    $caption = get_sub_field('content');
                    $image = get_sub_field('image');
              ?>
                <div class="row justify-content-between carousel-caption has-image">
                  <div class="col-12 col-sm-9">
                    <?php echo $caption; ?>
                  </div>

                  <div class="col-3 align-self-center d-none d-sm-block">
                    <img class="lozad" <?php acf\ar_responsive_image($image, 'thumb-640', '100%'); ?> alt="<?php echo get_the_title($image) ?>" >
                  </div>
                </div>

                <?php elseif ( get_row_layout() == 'caption_without_image' ) :
                  $caption = get_sub_field('content');
                ?>
                  <div class="row carousel-caption no-image">
                    <?php echo $caption; ?>
                  </div>
                <?php endif; //get_row_layout() == 'caption_with_image' ?>
              <?php endwhile; endif; //have_rows('caption') ) ?>

              <div class="carousel-control">
                <a class="carousel-control-prev" href="#page-carousel" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon fas fa-long-arrow-alt-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#page-carousel" role="button" data-slide="next">
                  <span class="carousel-control-next-icon fas fa-long-arrow-alt-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div> <!-- /.carousel-control -->

          </div> <!-- /.carousel-item -->
          <?php $count++; endwhile; //have_rows('gallery')?>

        </div> <!-- /.carousel-inner -->
      </div> <!-- /.carousel -->
    </section>
  </div>
<?php endif; //have_rows('gallery')?>
