<div class="container">
  <?php use Roots\Sage\acf;
    // check if the flexible content field has rows of data
    if ( have_rows('panels') ) :
      $count = 0;
      // loop through the rows of data
      while ( have_rows('panels') ) : the_row();
   ?>

     <?php
      // Left panel
       // check current row layout
       if( get_row_layout() == 'panel_left' ):

         $bg_colour = get_sub_field('background_colour');
         $panel_image = get_sub_field('image');
         $heading = get_sub_field('heading');
         $text_area = get_sub_field('text_area');
     ?>
    <section class="panel panel-left reveal move-up">
      <div class="row flex-md-row-reverse align-items-center panel-container">
        <div class="move-up col-11 col-sm-6 col-md-5 mx-auto p-0 ">
          <article class="panel__content">
            <h2 class="text-left"><?php echo $heading; ?></h2>
            <?php echo $text_area; ?>
          </article>

          <?php get_template_part('templates/components/panels', 'testimonial') ?>

        </div> <!-- /.col -->

        <?php if ( $panel_image ) : ?>
          <div class="move-up col-11 col-sm-6 col-md-5 mx-auto p-0 d-flex panel__image <?php if ( $count % 2 == 0 && $count != 0 ) { echo 'flex-row justify-content-md-start'; } else { echo 'flex-row-reverse justify-content-md-end'; }?>">
              <figure class="my-0">
                <img class="mw-100 lozad" <?php acf\ar_responsive_image($panel_image, 'thumb-1000', '1000px'); ?> alt="<?php echo get_the_title($panel_image); ?>" >
                <figcaption><?php if($count % 3 == 0) { echo '<span class="icon green-circle-2"></span>'; } else { echo '<span class="icon yellow-triangle-1"></span>';} ?><?php echo get_the_title($panel_image); ?></figcaption>
              </figure>
          </div><!-- /.col -->
        <?php endif; ?>
      </div><!-- /.row -->

      <span class="icon icon-reveal pink-plus-1"></span>
      <span class="icon icon-reveal blue-curve-1"></span>
      <span class="icon icon-reveal yellow-dash-4"></span>
      <span class="icon icon-reveal green-square-1"></span>
    </section>
  <?php endif; //get_row_layout() == 'panel_left' ?>

  <?php
   // Right panel
    // check current row layout
    if( get_row_layout() == 'panel_right' ):

      $bg_colour = get_sub_field('background_colour');
      $panel_image = get_sub_field('image');
      $heading = get_sub_field('heading');
      $text_area = get_sub_field('text_area');
  ?>

  <section class="panel panel-right reveal move-up">
    <div class="row align-items-center panel-container">
     <div class="move-up col-11 col-sm-6 col-md-5 mx-auto p-0">
       <article class="panel__content">
         <h2 class="text-left"><?php echo $heading; ?></h2>
         <?php echo $text_area; ?>
       </article>

       <?php get_template_part('templates/components/panels', 'testimonial') ?>

     </div> <!-- /.col -->

     <?php if ( $panel_image ) : ?>
       <div class="move-up col-11 col-sm-6 col-md-5 mx-auto p-0 d-flex flex-row-reverse panel__image">
           <figure class="my-0">
             <img class="mw-100 lozad" <?php acf\ar_responsive_image($panel_image, 'thumb-1000', '1000px'); ?> alt="<?php echo get_the_title($panel_image); ?>" >
             <figcaption><?php if($count % 2 == 0) { echo '<span class="icon pink-triangle-1"></span>'; } else { echo '<span class="icon blue-circle-2"></span>';} ?><?php echo get_the_title($panel_image); ?></figcaption>
           </figure>
       </div><!-- /.col -->
     <?php endif; ?>
    </div><!-- /.row -->

    <span class="icon icon-reveal yellow-curve-1"></span>
    <span class="icon icon-reveal green-curve-3"></span>
    <span class="icon icon-reveal purple-zigzag-1"></span>
    <span class="icon icon-reveal green-zigzag-1"></span>
    <span class="icon icon-reveal blue-plus-1"></span>
  </section>

  <?php endif; //get_row_layout() == 'panel_right' ?>

  <?php $count++; endwhile; endif; //have_rows('panels') ?>
</div>
