<ul class="entry-meta">
  <li><?php echo get_the_date('j M Y'); ?></li>

  <?php
    // Output each category as a list item with custom classes
    $categories = get_the_terms( $post->ID , 'news-category' );
    $separator = ' ';
    $output = '';

    if ( ! empty( $categories ) ) {
      foreach( $categories as $category ) {
          // Get ACF category colour
          $color = get_field('cat_colour', $category);
          // Output each category as a list item
          $output .= '<li class="cat-item bg-is-'. $color .'"><a href="' . esc_url( get_category_link( $category->term_id ) ) . '">' . esc_html( $category->name ) . '</a></li>' . $separator;
        }
      echo trim( $output, $separator );
    }
  ?>

</ul>
