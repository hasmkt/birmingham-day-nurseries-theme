<?php use Roots\Sage\acf; ?>
<section class="home-hero">
    <?php if ( have_rows('hero') ) : ?>

    <div id="carousel" class="carousel slide" data-ride="carousel">

      <ol class="carousel-indicators col-md-4 d-flex justify-content-center">
        <?php
          $count = 0;
          while ( have_rows('hero') ) : the_row();
        ?>
          <li data-target="#carousel" data-slide-to="<?php echo $count;?>" class="<?php if ( $count == 0 ) { echo 'active';} ?>"></li>
        <?php $count++; endwhile; ?>
      </ol>

      <div class="carousel-inner">
      <?php
        $count = 0;

        while ( have_rows('hero') ) : the_row();

          $bg_colour = get_sub_field('background_colour');
          $image = get_sub_field('image');
      ?>

      <div class="carousel-item bg-is-<?php echo $bg_colour; ?> <?php if ( $count == 0 ) { echo 'primary active';} ?>">
        <div class="container px-lg-0">

          <article class="row align-items-center">
            <?php if ( $image ) : ?>
              <div class="col-12 col-md-6 p-0 carousel-img__container">
                <img <?php acf\ar_responsive_image($image, 'thumb-640', '100%', false); ?> alt="<?php echo get_the_title($image) ?>" >
              </div>
            <?php endif; ?>

            <div class="col-12 col-md-6 mr-lg-0 carousel-content">
              <?php
                // Content area
                if ( have_rows('content_area') ) : while ( have_rows('content_area') ) : the_row();

                  $image = get_sub_field('image_optional');
                  $content = get_sub_field('text');
                ?>

                  <div class="col-sm-8 col-md-12 pb-5 pb-md-4 p-lg-0 pb-lg-6 <?php if ( $count != 0) { echo 'pt-3 pt-lg-4 px-lg-3 px-xl-0 pt-xl-0';} ?>">
                    <?php if ( $image ) : ?>
                      <img class="logo pb-lg-5" <?php acf\ar_responsive_image($image, 'thumb-640', '100%', false); ?> alt="<?php echo get_the_title($image) ?>" >
                    <?php endif; ?>
                    <p><?php echo $content; ?></p>
                  </div>

                <?php endwhile; endif; //have_rows('content_area') ?>

                <?php
                  // Button
                  if ( have_rows('button') ) : while ( have_rows('button') ) : the_row();

                    $text = get_sub_field('button_text');
                    $link = get_sub_field('button_link');
                ?>

                  <div class="col-sm-8 col-lg-10 pb-1 px-lg-0 <?php if ( $count != 0) { echo 'px-lg-3 px-xl-0';} ?>">
                    <?php if ( $text && $link ) : ?>
                        <a class="button b-is-<?php echo $bg_colour; ?>" href="<?php echo $link; ?>"><?php echo $text; ?></a>
                    <?php endif; ?>
                  </div>

                <?php endwhile; endif; //have_rows('button') ?>
            </div> <!-- /.carousel-content -->
          </article> <!-- /.row -->
        </div>  <!-- /.container -->
      </div> <!-- /.carousel-item -->

    <?php $count++; endwhile; endif; //have_rows('hero') ?>
    </div> <!-- /.carousel-inner -->
  </div> <!-- /.carousel -->
</section>
