<?php use Roots\Sage\acf; ?>
<div class="container">
  <section class="posts divider-before move-up">

    <div class="row">
      <div class="col-12">
        <header>
          <h2 class="title"><?php esc_html_e( 'From the blog', 'textdomain' ); ?></h2>
        </header>
      </div>
    </div>

      <?php get_template_part('templates/blog', 'latest-post-loop'); ?>

      <span class="icon icon-reveal pink-triangle-1"></span>
      <span class="icon icon-reveal green-line-1"></span>
      <span class="icon icon-reveal blue-zigzag-1"></span>
      <span class="icon icon-reveal yellow-square-1"></span>

  </section>
</div>
