<?php
$related = get_posts(
  array (
    'numberposts' => 3,
    'post__not_in' => array($post ->ID) //this will exclude the current post
    )
  );


  if( !empty($related) ) :
    ?>
  <section class="related-posts">
    <h2 class="title"><?php echo esc_html_e('You may also like', 'sage'); ?></h2>

    <div class="row justify-content-center mx-auto">
      <?php foreach($related as $post) : setup_postdata($post); ?>
        <div class="posts post-card col-12 col-sm-4 px-4 mb-5 mb-sm-7">
          <article <?php post_class('justify-content-md-between');?> >
            <header>

              <?php if ( has_post_thumbnail() ) : ?>
                <a href="<?php the_permalink(); ?>">
                  <figure>
                    <?php the_post_thumbnail(
                      'large',
                      array( 'class' => '')
                    ); ?>
                  </figure>
                </a>
              <?php endif;?>

              <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            </header>
            <footer>
              <?php get_template_part('templates/components/blog', 'list-categories') ?>
            </footer>

          </article>
        </div>
      <?php endforeach; wp_reset_postdata(); ?>
    </div>
  </section>
<?php endif; //!empty($related) ?>
