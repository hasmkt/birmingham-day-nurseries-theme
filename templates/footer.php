<?php use Roots\Sage\acf; ?>
<footer class="site-footer">
  <div class="container-fluid">
    <nav class="navbar p-md-0 pb-md-7">
      <div class="container-fluid p-md-0">

        <?php
          $image = get_field('logo_uob', 'option');

          if ( $image ) :
          ?>
          <a class="navbar-brand" target="_blank" href="<?= esc_url('https://www.birmingham.ac.uk/'); ?>">
            <img class="lozad" <?php acf\ar_responsive_image($image, 'thumb-640', '100%'); ?> alt="<?php echo get_the_title($image); ?>">
          </a>
        <?php endif; ?>

        <div class="secondary-menu p-md-0 ml-xl-auto">
          <?php
            if ( has_nav_menu('secondary_navigation') ) :
              wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav px-1 justify-content-center']);
            endif;
          ?>
        </div>

        <button class="to-top ml-lg-3 ml-xl-5" type="button" aria-label="Back to top">
          <span class="fas fa-chevron-up"></span>
        </button>
      </div> <!-- /.container-fluid -->
    </nav>


    <div class="contact-details">
      <div class="row flex-row justify-content-md-around justify-content-xl-end mx-auto">
        <div class="col-12 col-xl-3 px-sm-0 copyright-container">
          <p class="copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
        </div>

        <?php
        // Address
        if ( have_rows('contact_details', 'option') ) :
          while( have_rows('contact_details', 'option') ) : the_row();

          $the_elms   = get_sub_field('the_elms');
          $the_oaks   = get_sub_field('the_oaks');
          $the_maples = get_sub_field('the_maples');
        ?>

          <div class="col-12 col-xl-9 d-md-flex justify-content-md-around justify-content-xl-end px-sm-0">
            <?php
              if ( have_rows('opening_times', 'option') ) :
                while ( have_rows('opening_times', 'option') ) : the_row();

                $mon_fri = get_sub_field_object('monday_friday', 'option');
              ?>
              <div class="address d-md-inline-block mr-md-3">
                <h2><?php echo $the_elms['name']; ?></h4>
                <p><?php echo $the_elms['address']; ?></p>
                <p><?php echo $the_elms['phone_number']; ?></p>
                <?php echo '<p>' . $mon_fri['label'] . ' ' . $mon_fri['value'] . '</p>'; ?>
              </div>
              <img class="ofsted-logo mb-5 mb-sm-0 mr-md-5 lozad" src="<?php echo get_template_directory_uri() . '/assets/images/placeholder.png'?>" <?php acf\ar_responsive_image($the_elms['the_elms_ofsted'], 'thumb-150', '100px'); ?> alt="<?php echo get_the_title($the_elms['the_elms_ofsted']); ?>">
            <?php endwhile; endif; // opening_times ?>

            <?php
              if ( have_rows('opening_times_oaks', 'option') ) :
                while ( have_rows('opening_times_oaks', 'option') ) : the_row();

                $mon_fri = get_sub_field_object('monday_friday', 'option');
              ?>
              <div class="address d-md-inline-block mr-md-3">
                <h2><?php echo $the_oaks['name']; ?></h4>
                <p><?php echo $the_oaks['address']; ?></p>
                <p><?php echo $the_oaks['phone_number']; ?></p>
                <?php echo '<p>' . $mon_fri['label'] . ' ' . $mon_fri['value'] . '</p>'; ?>
              </div>
              <img class="ofsted-logo mb-5 mb-sm-0 mr-md-5 lozad" src="<?php echo get_template_directory_uri() . '/assets/images/placeholder.png'?>" <?php acf\ar_responsive_image($the_oaks['the_oaks_ofsted'], 'thumb-150', '100px'); ?> alt="<?php echo get_the_title($the_oaks['the_oaks_ofsted']); ?>">
            <?php endwhile; endif; // opening_times_oaks ?>

            <?php
              if ( have_rows('opening_times_maples', 'option') ) :
                while ( have_rows('opening_times_maples', 'option') ) : the_row();

                $mon_fri = get_sub_field_object('monday_friday', 'option');
              ?>
              <div class="address d-md-inline-block mr-md-3">
                <h2><?php echo $the_maples['name']; ?></h2>
                <p><?php echo $the_maples['address']; ?></p>
                <p><?php echo $the_maples['phone_number']; ?></p>
                <?php echo '<p>' . $mon_fri['label'] . ' ' . $mon_fri['value'] . '</p>'; ?>
              </div>
              <img class="ofsted-logo mb-5 mb-sm-0 lozad" src="<?php echo get_template_directory_uri() . '/assets/images/placeholder.png'?>" <?php acf\ar_responsive_image($the_maples['the_maples_ofsted'], 'thumb-150', '100px'); ?> alt="<?php echo get_the_title($the_maples['the_maples_ofsted']); ?>">
            <?php endwhile; endif; // opening_times_maples ?>
          </div>
        <?php endwhile;  endif; //have_rows('contact_details', 'option') ?>

      </div>
    </div> <!-- /.contact-details -->
  </div> <!-- /.container-fluid -->
</footer>
