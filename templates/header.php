<?php use Roots\Sage\extras; ?>
<header class="banner fixed-top bg-is-white pt-md-4">
  <div class="container-fluid">
    <nav class="navbar">

      <div class="container-fluid menu-top">
        <a class="login" href="<?php if (SwpmMemberUtils::is_member_logged_in()) { echo esc_url( get_page_link( 56 ) ); } else { echo esc_url( get_page_link( 351 ) ); } ?>">
          <span class="fas fa-user"></span> <?php esc_html_e( 'Parent Corner', 'textdomain' ); ?>
        </a>

        <?php if(SwpmMemberUtils::is_member_logged_in()) : ?>
          <a class="logout" href="<?= esc_url(home_url('/parent-corner/login/?swpm-logout=true')); ?>"><?php esc_html_e( 'Logout', 'textdomain' ); ?></a>
        <?php endif; ?>

      </div>

      <div class="container-fluid menu-bottom d-sm-flex">
        <div class="row justify-content-flex-end no-gutters">

            <div class="col-10 col-sm-5 col-md-6 col-xl-3">
              <?php get_template_part('templates/components/logo', ''); ?>
            </div>

            <div class="col-12 col-md-6 col-lg-6 d-none d-sm-none d-xl-block ml-lg-auto align-self-end">
              <div id="primaryNav" class="navbar d-lg-flex p-0">
                <?php
                  if ( has_nav_menu('primary_navigation') ) :
                    wp_nav_menu( array(
                      'theme_location' => 'primary_navigation',
                      'menu_class' => 'nav',
                      'depth' => 1 /*removes sub level menu in main menu*/
                    ) );
                  endif;
                ?>
              </div>
            </div>

            <div class="col-2 col-sm-7 col-md-6 col-xl-2 align-self-start align-self-md-end">
              <div class="d-flex justify-content-end">
                <div class="p-2">
                  <a class="button b-is-yellow d-none d-sm-inline-block " href="<?= esc_url( get_page_link( 35 ) ); ?>"><?php esc_html_e( 'Enquire now', 'textdomain' ); ?></a>
                </div>
                <div class="px-2 pb-2 p-sm-2 pr-md-0">
                  <button class="navbar-toggler" type="button" data-toggle="modal" data-target="#mainNav" aria-controls="mainNav" aria-label="Toggle navigation">
                    <span class="hamburger-inner"></span>
                  </button>
                </div>
              </div>

            </div>

        </div>
      </div>

    </nav>
  </div> <!-- /.container-fluid -->

  <?php 
    // Modal
    get_template_part('templates/components/modal', 'nav'); 

    // Notice
    get_template_part('templates/components/notice', '');
  ?>

</header>
