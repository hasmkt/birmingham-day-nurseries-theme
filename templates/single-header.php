<?php
  $categories = get_the_category();
  $separator = ' ';
  $output = '';
  $bg_colour = '';

  if ( ! empty( $categories ) ) {
      foreach( $categories as $category ) {
          // Get ACF category colour
          $color = get_field('cat_colour', $category);
          // Output colour
          $bg_colour .= 'bg-is-'. $color;
          $output .= '<li class="cat-item"><a href="' . esc_url( get_category_link( $category->term_id ) ) . '">' . esc_html( $category->name ) . '</a></li>' . $separator;
      }
  }
?>


  <div class="single-header row mx-0 flex-column-reverse flex-sm-row <?php echo $bg_colour; ?>">

    <div class="entry-title col-12 col-sm-6">
      <ul class="entry-meta">
        <?php echo trim( $output, $separator ); ?>
        <li><?php echo get_the_date('j M Y'); ?></li>
      </ul>

      <h1><?php the_title(); ?></h1>

      <span class="icon dark-circle-2"></span>
      <span class="icon dark-circle-2 alt"></span>
      <span class="icon dark-curve-1"></span>
      <span class="icon dark-curve-3"></span>
      <span class="icon dark-triangle-1"></span>
      <span class="icon dark-zigzag-1"></span>
      <span class="icon dark-dot-2"></span>
    </div>

    <div class="col-12 col-sm-6 p-0">
      <?php
      if ( has_post_thumbnail() ) : ?>

      <figure>
        <?php the_post_thumbnail(
          'large',
          array( 'class' => ''
            )
          ); ?>
      </figure>

      <?php endif;?>
    </div>

  </div>
