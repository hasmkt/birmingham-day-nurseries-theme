<?php use Roots\Sage\Titles; ?>

<div class="container-fluid blog-header bg-is-off-white">
  <header class="blog-title text-center">
    <div class="container">
      <h1 class="mb-0"><?= Titles\title(); ?></h1>
    </div>
    <span class="icon yellow-triangle-1"></span>
    <span class="icon blue-zigzag-1"></span>
    <span class="icon green-line-1"></span>
    <span class="icon pink-triangle-1"></span>
  </header>
</div>
