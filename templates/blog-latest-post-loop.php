<?php
  // Start a new loop and set posts per page to one to show only the latest post
  $the_query = new WP_Query( 'posts_per_page=1' );
  if( $the_query -> have_posts() ) :
  while ( $the_query -> have_posts() ) : $the_query -> the_post(); ?>
  <div class="latest-post posts col-12">
    <article <?php post_class('row');?> >
      <div class="col-12 col-sm-6 pl-sm-0 pr-sm-4 pr-lg-6">
        <header>
          <?php
          if ( has_post_thumbnail() ) : ?>

          <a href="<?php the_permalink(); ?>">
            <figure>
              <?php the_post_thumbnail(
                'large',
                array( 'class' => ''
                  )
                ); ?>
              </figure>
            </a>

          <?php endif;?>
        </header>
      </div>

      <div class="col-12 col-sm-6 pr-sm-0 pl-sm-4 pl-lg-6 mb-sm-3">
        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php the_excerpt(); ?>

        <footer>
          <?php get_template_part('templates/components/blog', 'list-categories') ?>
        </footer>
      </div>
    </article>
  </div>
<?php endwhile; endif; wp_reset_postdata();?>
