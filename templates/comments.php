<?php
if (post_password_required()) {
  return;
}
?>

    <button type="button" name="button" class="comment-toggle" data-toggle="collapse" data-target="#comments" aria-expanded="false" aria-controls="comments"> <?php echo 'Comments (' . get_comments_number() . ')'; ?> </button>

    <section id="comments" class="comments collapse">

    <?php if (have_comments()) : ?>
      <h2><?php printf(_nx('One response to &ldquo;%2$s&rdquo;', '%1$s responses to &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'sage'), number_format_i18n(get_comments_number()), '<span>' . get_the_title() . '</span>'); ?></h2>

      <ol class="comment-list">
        <?php wp_list_comments(['style' => 'ol', 'short_ping' => true]); ?>
      </ol>

      <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
        <nav>
          <ul class="pager">
            <?php if (get_previous_comments_link()) : ?>
              <li class="previous"><?php previous_comments_link(__('&larr; Older comments', 'sage')); ?></li>
            <?php endif; ?>
            <?php if (get_next_comments_link()) : ?>
              <li class="next"><?php next_comments_link(__('Newer comments &rarr;', 'sage')); ?></li>
            <?php endif; ?>
          </ul>
        </nav>
      <?php endif; ?>
    <?php endif; // have_comments() ?>

    <?php if (!comments_open() && get_comments_number() != '0' && post_type_supports(get_post_type(), 'comments')) : ?>
      <div class="alert alert-warning">
        <?php _e('Comments are closed.', 'sage'); ?>
      </div>
    <?php endif; ?>

</section>
<span class="far fa-comment"></span> <a class="add-comment-toggle" data-toggle="collapse" href="#commentform" role="button" aria-expanded="false" aria-controls="commentform">Add comment</a>

<?php
$args = array(
  'title_reply' => '',
  'class_form' => 'comment-form collapse'
);
comment_form($args);
?>
