  <div class="container">
    <section class="row justify-content-center">
      <article class="page-content col-11 px-0 pb-7">
        <?php the_content(); ?>
      </article>
    </section>
  </div>

    <?php get_template_part('templates/components/panels', ''); ?>
    <?php get_template_part('templates/components/nurseries', 'info'); ?>
    <?php get_template_part('templates/components/page', 'carousel'); ?>
    <?php get_template_part('templates/components/map'); ?>
    <?php get_template_part('templates/components/extend', 'content'); ?>
