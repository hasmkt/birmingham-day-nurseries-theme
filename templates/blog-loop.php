<div class="container-fluid">
  <div class="row">
    <?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php if ($wp_query->current_post == 0 ) : ?>
        <div class="latest-post w-100 bg-is-off-white">
          <div class="container">

            <article <?php post_class('row');?> >
              <div class="col-12 col-sm-6 pl-sm-0 pr-sm-4 pr-lg-6">
                <header>
                  <?php
                  if ( has_post_thumbnail() ) : ?>

                  <a href="<?php the_permalink(); ?>">
                    <figure>
                      <?php the_post_thumbnail(
                        'large',
                        array( 'class' => ''
                        )
                      ); ?>
                    </figure>
                  </a>

                  <?php endif;?>
                </header>
              </div>

              <div class="col-12 col-sm-6 pr-sm-0 pl-sm-4 pl-lg-6 mb-sm-3">
                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                <?php the_excerpt(); ?>

                <footer>
                  <?php get_template_part('templates/components/blog', 'list-categories') ?>
                </footer>
              </div>
            </article>

          </div> <!-- /.container -->
        </div> <!-- /.latest-post -->

        <div class="container">
          <div class="row">
            <?php else : ?>



              <div class="post-card col-12 col-sm-6 mb-5 mb-sm-7 <?php if( $wp_query->current_post % 2 == 0 ){echo 'pr-sm-0 pl-sm-4 pl-lg-6';} else {echo 'pl-sm-0 pr-sm-4 pr-lg-6';}?>">
                <article <?php post_class('justify-content-md-between');?> >
                  <header>

                    <?php if ( has_post_thumbnail() ) : ?>
                      <a href="<?php the_permalink(); ?>">
                        <figure>
                          <?php the_post_thumbnail(
                            'large',
                            array( 'class' => '')
                          ); ?>
                        </figure>
                      </a>
                    <?php endif;?>

                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                  </header>
                  <footer>
                    <?php get_template_part('templates/components/blog', 'list-categories') ?>
                  </footer>

                </article>
              </div>
          <?php endif; //$wp_query->current_post == 0 ?>
        <?php endwhile; endif; ?>
        </div> <!-- /.row -->
      </div> <!-- /.container -->
  </div> <!-- /.row -->
</div> <!-- /.container-fluid -->
