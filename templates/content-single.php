<header class="post-header">
  <?php get_template_part('templates/single', 'header'); ?>
</header>

<div class="container">
  <div class="row">
    <div class="col-12 col-sm-9 col-xl-7 mx-auto">
      <a class="breadcrumb-item" href="<?php echo esc_url( get_permalink(get_option( 'page_for_posts' )) );?>">
        <span class="fas fa-angle-left"></span><?php esc_html_e('Return to blog', 'sage'); ?>
      </a>

      <div class="post-entry-meta">
        <?php get_template_part('templates/components/blog', 'list-categories'); ?>
      </div>

      <div class="post-content">
        <?php the_content(); ?>
      </div>
    </div>
  </div>

  <div class="row">
    <footer class="post-footer col-12 col-sm-9 col-xl-7 mx-auto">

        <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
        <div>
          <a class="breadcrumb-item" href="<?php echo esc_url( get_permalink(get_option('page_for_posts')) );?>">
            <span class="fas fa-angle-left"></span><?php esc_html_e('Return to blog', 'sage'); ?>
          </a>
          <?php get_template_part('templates/components/blog', 'list-categories'); ?>
        </div>

      <?php comments_template('/templates/comments.php'); ?>
    </footer>
  </div>
</div>
