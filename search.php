<?php get_template_part('templates/page', 'header'); ?>

<?php get_template_part('templates/components/no', 'results') ?>

<?php while (have_posts()) : the_post(); ?>
  <div class="container-fluid">
    <section class="row justify-content-center mx-0">
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-9 col-xl-7 mx-auto">
            <?php get_template_part('templates/content', 'search'); ?>
          </div>
        </div>
      </div>
    </section>
  </div>
<?php endwhile; ?>

<?php get_template_part('templates/components/blog', 'pagination'); ?>
