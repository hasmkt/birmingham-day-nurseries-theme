<?php
/**
 * Template Name: Parent Corner Documents
 */
?>
<?php while (have_posts()) : the_post(); ?>
    <?php
      if (SwpmMemberUtils::is_member_logged_in()) :
        get_template_part('templates/page', 'header');
    ?>

    <section>
      <div class="container">
        <section>
          <?php if( have_rows('documents') ) : ?>
            <div class="row justify-content-md-between">
              <div class="col-12 px-sm-0">
                <ul class="documents-list d-flex flex-row flex-wrap">
                  <?php
                    while( have_rows('documents') ) : the_row();
                    $file = get_sub_field('file');
                  ?>
                      <li class="w-50"><a href="<?php echo $file['url']; ?>" target="_blank"><span class="far fa-file-alt"></span><?php echo $file['title']; ?></a></li>
                  <?php endwhile; // have_rows('documents') ?>
                </ul>
              </div>
            </div>
          <?php endif; // have_rows('documents') ?>
        </section>
      </div>

    <?php else : ?>

      <div class="container">
        <section class="row justify-content-center">
          <article class="page-content col-11 px-0 pb-7">

            <?php echo do_shortcode("[swpm_login_form]"); ?>

          </article>
        </section>
      </div>

    </section>
<?php endif; // is logged in?>

<?php endwhile; ?>
