<?php
/**
 * Template Name: Landing Page
 */
?>
<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>

  <?php get_template_part('templates/content', 'page'); ?>
  <?php get_template_part('templates/components/cards', 'intro'); ?>
  <?php get_template_part('templates/components/cards', 'landing'); ?>

<?php endwhile; ?>
