<?php
$auth = SwpmAuth::get_instance();
$setting = SwpmSettings::get_instance();
$password_reset_url = $setting->get_value('reset-page-url');
$join_url = $setting->get_value('join-us-page-url');
?>
<div class="swpm-login-widget-form">
  <form id="swpm-login-form" name="swpm-login-form" method="post" action="">
    <h3>Login</h3>
    <div class="form-group swpm-username">
      <label for="swpm_user_name" class="swpm-username-label swpm-label"><?php echo SwpmUtils::_('Username or Email') ?></label>
      <input type="text" class="swpm-text-field swpm-username-input swpm-username-field" id="swpm_user_name" value="" size="25" name="swpm_user_name" />
    </div>

    <div class="form-group swpm-password">
        <label for="swpm_password" class="swpm-password-label swpm-label"><?php echo SwpmUtils::_('Password') ?></label>
        <input type="password" class="swpm-text-field swpm-password-input swpm-password-field" id="swpm_password" value="" size="25" name="swpm_password" />
    </div>

    <div class="form-group custom swpm-remember-me">
        <input id="rememberme" type="checkbox" name="rememberme" value="checked='checked'">
        <label class="swpm-rember-label" for="rememberme"> <?php echo SwpmUtils::_('Remember Me') ?></label>
    </div>

    <div class="swpm-before-login-submit-section"><?php echo apply_filters('swpm_before_login_form_submit_button', ''); ?></div>

    <div class="swpm-login-submit">
        <input type="submit" class="swpm-login-form-submit" name="swpm-login" value="<?php echo SwpmUtils::_('Login') ?>"/>
    </div>
    <div class="swpm-forgot-pass-link">
        <a id="forgot_pass" class="swpm-login-form-pw-reset-link"  href="<?php echo $password_reset_url; ?>"><?php echo SwpmUtils::_('Forgot Password') ?>?</a>
    </div>

    <div class="swpm-login-action-msg">
        <span class="swpm-login-widget-action-msg"><?php echo $auth->get_message(); ?></span>
    </div>
  </form>
</div>
