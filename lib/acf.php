<?php

namespace Roots\Sage\acf;

if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
		'page_title' 	=> 'Site Settings',
		'menu_title'	=> 'Site Settings',
		'menu_slug' 	=> 'site-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}

/**
 * Responsive Image Helper Function
 *
 * @param string $image_id the id of the image (from ACF or similar)
 * @param string $image_size the size of the thumbnail image or custom image size
 * @param string $max_width the max width this image will be shown to build the sizes attribute
 * @param bool $lazyLoad whether we want to lazy load the image or not. Default is true
 * http://aaronrutley.com/responsive-images-in-wordpress-with-acf/
 */

function ar_responsive_image($image_id,$image_size,$max_width, $lazyLoad = true){

	// check the image ID is not blank
	if($image_id != '') {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		// generate the markup for the responsive image
		if ($lazyLoad == true) {
			echo 'data-src="'.$image_src.'" data-srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';
		} else {
			echo 'src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';
		}

	}
}

// set API key for Google Map
function my_acf_google_map_api( $api ){

	$api['key'] = 'AIzaSyBYQRXmsVXaWCuqu_CFeD7iQTYlhja2zi8';

	return $api;

}

add_filter('acf/fields/google_map/api', __NAMESPACE__ . '\\my_acf_google_map_api');
