<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }
  // Add scroll reveal class to pages
  if (is_single() || is_page()) {
    $classes[] = 'scroll-reveal';
  }
  // Add has latest posts class to home page and parent corner
  if (is_front_page() || is_page(56)) {
    $classes[] = 'has-latest-posts';
  }
  // Add class if cards exist on a page
  if( have_rows('cards') ) {
    $classes[] = 'has-cards';
  }
  // Add class if notice is active
  if ( have_rows('notice', 'option') ) {
    while ( have_rows('notice', 'option') ){
      the_row();
      $notice_active = get_sub_field('notice_active', 'option');
    }
    if ($notice_active) {
      $classes[] = 'notice-active';
    }
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Add class to menu <li> and <a> tags
 */

// <li>
function add_class_to_menu_item( $classes, $item, $args ) {
    // add 'nav-item' to classes array
    $classes[] = 'nav-item';

    // add active class to active page
    if ( in_array('current-menu-item', $classes) ) {
      $classes[] = 'active';
    }

    return $classes;
}
add_filter( 'nav_menu_css_class', __NAMESPACE__ . '\\add_class_to_menu_item', 10, 3 );

// <a>
function add_class_to_menu_anchor( $atts, $item, $args ) {
  // add 'nav-link' to class attribute
    $atts['class'] = 'nav-link';
    return $atts;
}
add_filter( 'nav_menu_link_attributes', __NAMESPACE__ . '\\add_class_to_menu_anchor', 10, 3 );


// Add opening times to secondary menu
// function custom_menu_item( $items, $args ) {
//
//   if ( $args->theme_location == 'secondary_navigation' ) {
//     // get opening times from site options page
//     $opening_times  = get_field('opening_times', 'option');
//
//     $new_item       = array('<li class="menu-item opening-times">Opening Times ' . $opening_times['monday_friday'] . '</li>');
//     $items          = preg_replace( '/<\/li>\s<li/', '</li>,<li',  $items );
//
//     $array_items    = explode( ',', $items );
//     array_splice( $array_items, 0, 0, $new_item ); // splice in at start
//
//     $items          = implode( '', $array_items );
//
//   }
//   return $items;
//   var_dump($new_item);
// }
// add_filter( 'wp_nav_menu_items', __NAMESPACE__ . '\\custom_menu_item', 10, 2 );

// Custom pagination
// Add class to get_next_posts_link
add_filter('next_posts_link_attributes', __NAMESPACE__ . '\\posts_link_attributes');
add_filter('previous_posts_link_attributes', __NAMESPACE__ . '\\posts_link_attributes');

function posts_link_attributes() {
    return 'class="page-link"';
}

// Create custom pagination
// From http://www.wpbeginner.com/wp-themes/how-to-add-numeric-pagination-in-your-wordpress-theme/
// Edited to include bootstrap classes
function wpbeginner_numeric_posts_nav() {
    if( is_singular() )
        return;

    global $wp_query;
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<nav aria-label="Page navigation"><ul class="pagination">' . "\n";
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf('<li class="page-item previous">%s</li>' . "\n", get_previous_posts_link( __('<span class="fas fa-angle-left"></span><span class="sr-only">Previous</span>', 'sage') ));
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="page-item active"' : '';

        printf( '<li%s class="page-item"><a class="page-link" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li class="page-item">…</li>';
    }
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="page-item active"' : '';
        printf( '<li%s class="page-item"><a class="page-link" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li class="page-item">…</li>' . "\n";

        $class = $paged == $max ? ' class="page-item active"' : '';
        printf( '<li%s class="page-item"><a class="page-link" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf('<li class="page-item next">%s</li>' . "\n", get_next_posts_link( __('<span class="fas fa-angle-right"></span><span class="sr-only">Next</span>', 'sage') ));

    echo '</ul></nav>' . "\n";
}



// Get submenu items from wp menu based on parent or sibling

add_filter( 'wp_nav_menu_objects', __NAMESPACE__ . '\\my_wp_nav_menu_objects_sub_menu', 10, 2 );
// filter_hook function to react on sub_menu flag
function my_wp_nav_menu_objects_sub_menu( $sorted_menu_items, $args ) {
  if ( isset( $args->sub_menu ) ) {
    $root_id = 0;

    // find the current menu item
    foreach ( $sorted_menu_items as $menu_item ) {
      if ( $menu_item->current ) {
        // set the root id based on whether the current menu item has a parent or not
        $root_id = ( $menu_item->menu_item_parent ) ? $menu_item->menu_item_parent : $menu_item->ID;
        break;
      }
    }

    // find the top level parent
    if ( ! isset( $args->direct_parent ) ) {
      $prev_root_id = $root_id;
      while ( $prev_root_id != 0 ) {
        foreach ( $sorted_menu_items as $menu_item ) {
          if ( $menu_item->ID == $prev_root_id ) {
            $prev_root_id = $menu_item->menu_item_parent;
            // don't set the root_id to 0 if we've reached the top of the menu
            if ( $prev_root_id != 0 ) $root_id = $menu_item->menu_item_parent;
            break;
          }
        }
      }
    }
    $menu_item_parents = array();
    foreach ( $sorted_menu_items as $key => $item ) {
      // init menu_item_parents
      if ( $item->ID == $root_id ) $menu_item_parents[] = $item->ID;
      if ( in_array( $item->menu_item_parent, $menu_item_parents ) ) {
        // part of sub-tree: keep!
        $menu_item_parents[] = $item->ID;
      } else if ( ! ( isset( $args->show_parent ) && in_array( $item->ID, $menu_item_parents ) ) ) {
        // not part of sub-tree: away with it!
        unset( $sorted_menu_items[$key] );
      }
    }

    return $sorted_menu_items;
  } else {
    return $sorted_menu_items;
  }
}

/**
 * Use Lozad (lazy loading) for attachments/featured images
 */
// add_filter('wp_get_attachment_image_attributes', function ($attr, $attachment) {
//   // Bail on admin
//   if (is_admin()) {
//       return $attr;
//   }

//   $attr['data-src'] = $attr['src'];
//   $attr['data-srcset'] = $attr['srcset'];
//   $attr['class'] .= ' lozad';
//   unset($attr['src']);
//   unset($attr['srcset']);

//   return $attr;
// }, 10, 2);