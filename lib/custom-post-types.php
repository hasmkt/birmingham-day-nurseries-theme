<?php
namespace Roots\Sage\Custom_Post_Types;
/**
* Register a News item post type.
*
* @link http://codex.wordpress.org/Function_Reference/register_post_type
*/
add_action( 'init', __NAMESPACE__ . '\\parent_news' );
function parent_news() {
	$labels = array(
		'name'               => _x( 'News', 'post type general name', 'sage' ),
		'singular_name'      => _x( 'News', 'post type singular name', 'sage' ),
		'menu_name'          => _x( 'Parent news items', 'admin menu', 'sage' ),
		'name_admin_bar'     => _x( 'Parent news item', 'add new on admin bar', 'sage' ),
		'add_new'            => _x( 'Add New', 'news', 'sage' ),
		'add_new_item'       => __( 'Add New news item', 'sage' ),
		'new_item'           => __( 'New news item', 'sage' ),
		'edit_item'          => __( 'Edit news item', 'sage' ),
		'view_item'          => __( 'View news item', 'sage' ),
		'all_items'          => __( 'All news items', 'sage' ),
		'search_items'       => __( 'Search news items', 'sage' ),
		'parent_item_colon'  => __( 'Parent news items:', 'sage' ),
		'not_found'          => __( 'No News items found.', 'sage' ),
		'not_found_in_trash' => __( 'No News items found in Trash.', 'sage' )
	);

	$args = array(
		'labels'             => $labels,
    'description'        => __( 'Description.', 'sage' ),
		'public'             => true,
		'publicly_queryable' => true,
    'exclude_from_search'=> false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array(
															'with_front' 	=> false,
															'slug' 				=> 'parent-corner/news'
		 												),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 5,
    'menu_icon'          => 'dashicons-format-aside',
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields', 'excerpt', 'comments' )
	);

	register_post_type( 'parent news', $args );
}

register_taxonomy( 'news-category',
	array('parentnews'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
	array('hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __( 'Categories', 'sage' ), /* name of the custom taxonomy */
			'singular_name' => __( 'Category', 'sage' ), /* single taxonomy name */
			'search_items' =>  __( 'Search Categories', 'sage' ), /* search title for taxomony */
			'all_items' => __( 'All Categories', 'sage' ), /* all title for taxonomies */
			'parent_item' => __( 'Parent Category', 'sage' ), /* parent title for taxonomy */
			'parent_item_colon' => __( 'Parent Category:', 'sage' ), /* parent taxonomy title */
			'edit_item' => __( 'Edit Category', 'sage' ), /* edit custom taxonomy title */
			'update_item' => __( 'Update Category', 'sage' ), /* update title for taxonomy */
			'add_new_item' => __( 'Add New Category', 'sage' ), /* add new title for taxonomy */
			'new_item_name' => __( 'New Category Name', 'sage' ) /* name title for taxonomy */
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'parent-corner/news' ),
	)
);


?>
