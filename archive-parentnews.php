<?php use Roots\Sage\Extras;

get_template_part('templates/blog', 'header'); ?>

<section class="posts">
  <?php get_template_part('templates/components/no', 'results'); ?>

  <?php
    get_template_part('templates/parentnews-loop', get_post_type() != 'post' ? get_post_type() : get_post_format());

    get_template_part('templates/components/blog', 'pagination');
   ?>

</section>
