<?php while (have_posts()) : the_post(); ?>

    <?php
      get_template_part('templates/components/home', 'hero');
      get_template_part('templates/components/home', 'intro');
      get_template_part('templates/components/panels', '');
      get_template_part('templates/components/cards', '');
      get_template_part('templates/components/extend', 'content');
      get_template_part('templates/components/blog', 'feature');
     ?>

<?php endwhile; ?>
