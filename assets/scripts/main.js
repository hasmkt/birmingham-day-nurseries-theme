/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        // init lazy load
        const observer = lozad(); // lazy loads elements with default selector as '.lozad'
        observer.observe();
        // const coolImage = document.querySelector('.image-to-load-first');
        // // ... trigger the load of a image before it appears on the viewport
        // observer.triggerLoad(coolImage);

        // Scroll to top
        (function() {
          $('.to-top').on('click', function() {
            $('html, body').animate({scrollTop: 0}, 2000);
            return false;
          });
        })();

        // set cookie
        function setCookie(cname, cvalue, exdays) {
          var d = new Date();
          d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
          var expires = "expires=" + d.toUTCString();
          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

      // fade in callout and set cookie
        jQuery(function($) {
          var notice = $('.notice'),
              close   = $('.notice .close');

          // set cookie to last one day
          close.click(function() {
            notice.fadeOut('linear');
            setCookie('dismissed-notice', 'yes', 1);
          });

        });
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // Applying for a place page
    'page_template_page_apply': {
      init: function() {
        // JavaScript to be fired on the applying for a place page

        // Use setInterval to check if NF form has loaded
        (function() {
          var formExists = setInterval(function() {

            /**
              * Highlight the checkbox container when a checkbox is selected
              */

            var checkboxContainer = $('.listcheckbox-container'),
                checkbox          = $('.list-checkbox-wrap input[type="checkbox"]'),
                radio             = $('.list-radio-wrap input[type="radio"]'),
                fullTime          = $('#nf-field-149-0'),
                partTime          = $('#nf-field-149-1'),
                termTime          = $('#nf-field-149-2');

            // check for the listcheckbox-container
            if ( checkboxContainer.length ) {

              // Check for change of state on checkbox
              checkbox.change(function() {
                // If checkbox is checked add b-is-yellow class to parent listcheckbox-container
                $(this).parents(checkboxContainer).toggleClass('b-is-yellow', checkbox.is(':checked'));
              });

              // Check for change of state on radio
              radio.change(function() {
                if (fullTime.is(':checked')) {
                  checkboxContainer.addClass('b-is-yellow');
                } else if (partTime.is(':checked') || termTime.is(':checked')) {
                  checkboxContainer.removeClass('b-is-yellow');
                }
              });

              /**
                * Disable checkboxes until user has scrolled through the t&c's
                */
                var disabled    = $('.disabled .nf-element'),
                    termsInner  = $('.terms-inner');

                disabled.attr('disabled', true);

                // when user scrolls to bottom of terms-inner change the disabled attr to false
                termsInner.on('scroll', function() {
                  if ( $(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight ) {

                    $(this).closest('.terms').closest('nf-field').next().find(disabled).attr('disabled', false);

                  }
                });



              clearInterval(formExists);
            }
          }, 100); // check every 100ms
        })();

      }
    },
    // Scroll Reveal
    'scroll_reveal': {
      init: function() {
        (function() {
          // init scroll reveal(sr) and reset the defaults
          // Docs: https://github.com/jlmakes/scrollreveal
          window.sr = new ScrollReveal({reset: true});
          // call sr on element and customise reveal set
            // element is selected in the same way as you would select on jquery

          sr.reveal('.icon-reveal', {
            origin:   'bottom',
            distance: '7rem',
            duration: 800,
            rotate: {
              z: 0
            },
            delay:    0.9,
            useDelay: 'always',
            viewFactor: 0.1,
            opacity:  0,
            scale:    0.1,
            easing:   'cubic-bezier(0.2, 1, 0.22, 1)'
          }, 100);

          sr.reveal('.fade-in', {
            origin:   'bottom',
            duration: 700,
            viewFactor: 0.1,
            scale:    1,
            distance: '0',
            reset: false,
            opacity:  0,
            easing:   'linear'
          });

          sr.reveal('.move-up', {
            origin:   'bottom',
            distance: '4rem',
            duration: 1000,
            delay:    0.1,
            viewFactor: 0.1,
            reset: false,
            opacity:  0,
            scale:    0.9,
            easing:   'ease'
          });

          sr.reveal('.move-up-reset', {
            origin:   'bottom',
            distance: '4rem',
            duration: 1000,
            delay:    0.1,
            viewFactor: 0.1,
            opacity:  0,
            scale:    0.9,
            easing:   'ease'
          });

        })();
      }
    },

    // Cards hover
    'has_cards': {
      init: function() {
        (function() {
          // when hover on a card move the icons within it
          $('.card').each(function() {
            $(this).hover(
              function() {
                $(this).find('.icon').addClass('icon-hover-move');
              },
              function() {
                $(this).find('.icon').removeClass('icon-hover-move');
              }
            );
          });
        })();
      }
    },
    // Post pages
    'single': {
      init: function() {
        (function() {
          var replyLink   = $('.comment-reply-link'),
              cancelReply = $('#cancel-comment-reply-link'),
              commentForm = $('#commentform');

          replyLink.click(function() {
            commentForm.collapse('toggle');
          });
          cancelReply.click(function() {
            commentForm.collapse('hide');
          });
        })();
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
