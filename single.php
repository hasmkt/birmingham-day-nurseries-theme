<?php while (have_posts()) : the_post(); ?>
  <div class="container-fluid">
    <section class="row justify-content-center mx-0">
      <article <?php post_class('w-100'); ?>>

        <?php get_template_part('templates/content-single', get_post_type()); ?>

      </article>
    </section>

    <?php get_template_part('templates/content-single', 'related'); ?>
  </div>
<?php endwhile; ?>
