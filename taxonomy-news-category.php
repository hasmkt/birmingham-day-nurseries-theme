<?php
/*
This is the custom post type taxonomy template.
If you edit the custom taxonomy name, you've got
to change the name of this template to
reflect that name change.

i.e. if your custom taxonomy is called
register_taxonomy( 'shoes',
then your single template should be
taxonomy-shoes.php

*/

use Roots\Sage\Extras; ?>

<?php get_template_part('templates/blog', 'header'); ?>

<section class="posts">
  <?php get_template_part('templates/components/no', 'results'); ?>

  <?php
    get_template_part('templates/parentnews-loop', get_post_type() != 'post' ? get_post_type() : get_post_format());

    get_template_part('templates/components/blog', 'pagination');
   ?>

</section>
